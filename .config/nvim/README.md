## Installation

### Neovim

```sh
git clone --depth 1 https://github.com/neovim/neovim.git
cd .deps
cmake ../third-party
make
cd ../build
cmake ..
make
sudo make install

sudo pacman -U /var/cache/pacman/pkg/libffi-*
pip install neovim
npm i -g neovim
```

### Plugins

```vim
:PlugInstall
:CocInstall coc-json
:CocInstall coc-actions
:CocInstall coc-html
:CocInstall coc-rls
:CocInstall coc-flutter
:CocInstall coc-snippets vim-go
:CocInstall @yaegassy/coc-volar coc-eslint coc-prettier coc-tsserver
```

### Third party commands:

```sh
go get golang.org/x/tools/gopls
npm i -g eslint eslint-plugin-vue

git clone --depth 1 https://github.com/rust-analyzer/rust-analyzer && cd rust-analyzer
cargo xtask install --server

rustup component add rls rust-analysis rust-src

npm install -g standard
```

### Go

```
go install github.com/fatih/gomodifytags@latest
go install github.com/fatih/motion@latest
go install github.com/jstemmer/gotags@latest

,m  # TagbarToggle
```



## Usage

### General

- F3:  Open NERDTree
- F2:  Show current dir in NERDTree
- F4:  Open tags definition
- ,y:  Vim commands history
- YY:  Copy to clipboard
- XX:  Cut to clipboard
- ,gp: Paste from clipboard
- ,ci: Toggle commentary
- ,h:  Split horizontally. (ctrl+w j to focus)
- ,v:  Split vertically. (ctrl+w l to focus)
- ,.:  Set current dir as working dir
- ,sh: Terminal emulator
- ,e:  Opens an edit command with the path of the currently edited file filled in
- ,te: Opens an tabedit command with the path of the currently edited file filled in
- ,  : Clear search
- ,fe: Run FZF

### Git

- ,gb: Git blame
- ,gd: Git diff
- ,o : View line in GitHub

### Session

- ,ss <name>: Save session
- ,so <name>: Open session
- ,sd <name>: Delete session
- ,sc <name>: Close session
- ,f <word>:  Recursive grep

### Tabs
- Tab: Next tab
- Shift+Tab: Previous tab
- Shift+H: Move tab to left
- Shift+L: Move tab to right

### Buffers

- ,b: List
- ,c: Close

### Multi cursor

- Ctrl+n: Select a word and next equal words
- Ctrl+x: Skip next equal word
- Ctrl+b: Select previous equal word
