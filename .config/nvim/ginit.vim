let ayucolor="dark"
colorscheme ayu
" colorscheme base16-dracula

" Guifont DejaVu Sans Mono:h15
" Guifont JetBrains Mono:h15
Guifont Inconsolata:h17

GuiTabline 1
let g:GuiWindowMaximized = 1

set clipboard=unnamedplus
vmap <LeftRelease> " *ygv

" Ctrl+c - copiar
vmap <C-c> "+yi

" Ctrl+x - recortar
vmap <C-x> "+c

" Ctrl+v colar
vmap <C-v> c<ESC>"+p
imap <C-v> <C-r><C-o>+

" Botao direito do mouse exibe menu
nnoremap <silent><RightMouse> :call GuiShowContextMenu()<CR>
inoremap <silent><RightMouse> <Esc>:call GuiShowContextMenu()<CR>
vnoremap <silent><RightMouse> :call GuiShowContextMenu()<CR>gv
