let vimplug_exists=expand('~/.config/nvim/autoload/plug.vim')
let g:python_host_prog = $HOME.'/.pyenv/shims/python'
let g:python3_host_prog = $HOME.'/.pyenv/shims/python'
" let g:python3_host_prog = '~/.pyenv/shims/python'
" let g:python3_host_prog = '/usr/bin/python3'

"" [config] vim-polyglot
set nocompatible
let g:polyglot_disabled = ['python', 'dart', 'clojure', 'typescript']

filetype plugin indent on

if !filereadable(vimplug_exists)
  if !executable("curl")
    echoerr "You have to install curl or first install vim-plug yourself!"
    execute "q!"
  endif
  echo "Installing Vim-Plug..."
  echo ""
  silent exec "!\curl -fLo " . vimplug_exists . " --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  let g:not_finish_vimplug = "yes"

  autocmd VimEnter * PlugInstall
endif

call plug#begin(stdpath('config') . '/plugged')

" Better replace string
" Ex.: :%Subvert/facilit{y,ies}/building{,s}/g
Plug 'tpope/vim-abolish'

" File explorer
" Press F2 or F3
Plug 'preservim/nerdtree'

" A Git wrappe
" :GBrowse, :Gblame, :Gdiff, etc
Plug 'tpope/vim-fugitive'

" lean & mean status/tabline for vim that's light as air 
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Plug 'vim-scripts/grep.vim'            " It allows you to search for a pattern in one or more files

" Displays tags in a window running ctags command
" nmap <F8> :TagbarToggle<CR>
Plug 'preservim/tagbar'

" Languages
Plug 'dart-lang/dart-vim-plugin'
Plug 'thosakwe/vim-flutter'
Plug 'hail2u/vim-css3-syntax'
Plug 'gorodinskiy/vim-coloresque'         " Color preview
Plug 'tpope/vim-haml'                     " Vim runtime files for Haml, Sass, and SCSS
Plug 'mattn/emmet-vim'                    " Expanding abbreviations similar to emmet
Plug 'raimon49/requirements.txt.vim', {'for': 'requirements'}  " Requirements File Format syntax support
Plug 'racer-rust/vim-racer'               " Racer for Rust code completion. cargo install racer
Plug 'rust-lang/rust.vim'                 " Rust file detection, syntax highlighting, formatting etc.
Plug 'jelera/vim-javascript-syntax'       " Enhanced javascript syntax file
Plug 'neoclide/coc-tsserver', {'do': 'yarn install --frozen-lockfile'}
Plug 'yaegassy/coc-volar', { 'do': 'yarn install --frozen-lockfile' }
Plug 'yaegassy/coc-volar-tools', { 'do': 'yarn install --frozen-lockfile' }
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

" A solid language pack for Vim
Plug 'sheerun/vim-polyglot'

" Vim plugin for intensely nerdy commenting powers
" <leader>ci or <leader>cs or <leader>cl
Plug 'preservim/nerdcommenter'

" Extends vim %
" Plug 'andymass/vim-matchup'            " Navigate and highlight matching words

Plug 'terryma/vim-multiple-cursors'

" The undo history visualizer
" nnoremap <F5> :UndotreeToggle<CR>
Plug 'mbbill/undotree'

" Fades your inactive buffers and preserves your syntax highlighting
Plug 'TaDaa/vimade'

" Load extensions like VSCode and host language servers
Plug 'neoclide/coc.nvim', {'branch': 'release'}
let g:coc_global_config="~/.config/nvim/coc-settings.json"

" Fuzzy finder
" Requires fzf, bat, delta, rg. <leader><space>
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" :mksession on steroids
" :SessionSave, SessionLoad
" Plug 'olimorris/persisted.nvim'

" The ultimate snippet solution for Vim
" Requires: pip3 install neovim
Plug 'SirVer/ultisnips'                          " The ultimate snippet solution for Vim

" Asynchronous completion framework
" pip3 install --user --upgrade pynvim. #desligado
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
let g:deoplete#enable_at_startup = 0


"" Themes
Plug 'junegunn/seoul256.vim', { 'dir': stdpath('config') . '/colors/seoul256' }
Plug 'sjl/badwolf', { 'dir': stdpath('config') . '/colors/badwolf' }
Plug 'danilo-augusto/vim-afterglow', { 'dir': stdpath('config') . '/colors/afterglow' }

" Http Client used by Mia
Plug 'nvim-lua/plenary.nvim'

call plug#end()

"*****************************************************************************
"" Look and Feel
"*****************************************************************************"
syntax on                   " syntax highlighting
set number                  " add line numbers
set ruler
set gcr=a:blinkon0
set scrolloff=3
set laststatus=2
set title
set titleold="Terminal"
set titlestring=%F

" set statusline=[%l/%L][%p%%][%c]\ %=\ %r\ %y\ %F
if exists("*fugitive#statusline")
  set statusline+=%{fugitive#statusline()}
endif

"" Themes
let g:seoul256_background = 233
let g:gruvbox_contrast_dark = "hard"
let g:badwolf_tabline = 0
let g:badwolf_css_props_highlight = 1
let g:afterglow_blackout=1
silent! colorscheme afterglow

"*****************************************************************************
"" Basic Setup
"*****************************************************************************"

"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set fileformats=unix,dos,mac

"" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase

set sessionoptions+=winpos,terminal,folds
set termguicolors
set nocompatible            " disable compatibility to old-time vi
set showmatch               " show matching brackets.
set ignorecase              " case insensitive matching
set mouse=a                 " middle-click paste with mouse
set hlsearch                " highlight search results
set tabstop=4               " number of columns occupied by a tab character
set softtabstop=4           " see multiple spaces as tabstops so <BS> does the right thing
set expandtab               " converts tabs to white space
set shiftwidth=4            " width for autoindents
set autoindent              " indent a new line the same amount as the line just typed
set wildmode=longest,list,full " get bash-like tab completions
" set cc=120                " set an 80 column border for good coding style
set lazyredraw              " redraw only when we need to.
set backspace=indent,eol,start
set noswapfile
set nowrap
set cursorline
set wildmenu
set wildignorecase
set foldenable
set foldlevelstart=30
set foldnestmax=40
set foldmethod=indent
set hidden
set nobackup
set nowritebackup
if has('nvim')
  set ttimeout
  set ttimeoutlen=0
endif

let g:mapleader = ','
let g:tablineclosebutton=0

"*****************************************************************************
"" Plugins Setup
"*****************************************************************************"

let g:mia_api_url='http://localhost:8080/ai'
let g:mia_override_selected=1
let g:mia_model='deepseek-coder:1.3b'
nnoremap <silent> <leader>ai :Mia<CR>
vnoremap <silent> <leader>ai :Mia<CR>


"" [config] vim-airline
" let g:airline_theme = 'simple'
let g:airline_theme = 'afterglow'
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#tabline#enabled = 0
let g:airline_powerline_fonts = 1
let g:airline#extensions#tagbar#enabled = 0
let g:airline#extensions#tabline#left_sep = '▶'
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'default' " jsformatter, unique_tail, unique_tail_improved
let g:airline#extensions#virtualenv#enabled = 0
let g:airline_skip_empty_sections = 1
let g:airline_section_error = '%{airline#util#wrap(airline#extensions#coc#get_error(),0)}'
let g:airline_section_warning = '%{airline#util#wrap(airline#extensions#coc#get_warning(),0)}'

"" [config] vim-multiple-cursors
let g:multi_cursor_use_default_mapping = 1
let g:multi_cursor_quit_key            = '<Esc>'
highlight multiple_cursors_cursor term=reverse cterm=reverse gui=reverse
highlight link multiple_cursors_visual Visual

"" [config] nerdcommenter
let g:NERDSpaceDelims = 1
let g:NERDCommentEmptyLines = 1
let g:NERDCompactSexyComs = 1
let g:NERDCustomDelimiters = {
  \ 'vue': { 'left': '//', 'leftAlt': '/*','rightAlt': '*/' },
  \ 'html': { 'left': '<!--','right': '-->' },
  \ 'javascript': { 'left': '//', 'leftAlt': '/*', 'rightAlt': '*/' }
\}

"" [config] vim-fugitive
noremap <Leader>gl :Git log<CR>
noremap <Leader>gds :Gvdiffsplit<CR>
noremap <Leader>gs :Git<CR>
noremap <Leader>gb :Git blame<CR>
noremap <Leader>gd :Git diff<CR>

"" [config] tagbar
nmap <leader>m :TagbarOpenAutoClose<CR>
" nmap <leader>m :TagbarToggle<CR>
let g:tagbar_autofocus = 1
let g:tagbar_type_json = {
    \ 'ctagstype' : 'json',
    \ 'kinds' : [
      \ 'o:objects',
      \ 'a:arrays',
      \ 'n:numbers',
      \ 's:strings',
      \ 'b:booleans',
      \ 'z:nulls'
    \ ],
  \ 'sro' : '.',
    \ 'scope2kind': {
    \ 'object': 'o',
      \ 'array': 'a',
      \ 'number': 'n',
      \ 'string': 's',
      \ 'boolean': 'b',
      \ 'null': 'z'
    \ },
    \ 'kind2scope': {
    \ 'o': 'object',
      \ 'a': 'array',
      \ 'n': 'number',
      \ 's': 'string',
      \ 'b': 'boolean',
      \ 'z': 'null'
    \ },
    \ 'sort' : 0
    \ }

"" [config] undotree
nnoremap <F5> :UndotreeToggle<CR>

"" [config] auto-session
nnoremap <leader>so :OpenSession<Space>
nnoremap <leader>ss :SaveSession<Space>
nnoremap <leader>sd :DeleteSession<CR>
nnoremap <leader>sc :CloseSession<CR>

"" [config] nerdtree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
let g:NERDTreeChDirMode=2
let g:NERDTreeIgnore=['\.rbc$', '\~$', '\.pyc$', '\.db$', '\.sqlite$', '__pycache__']
let g:NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$', '\.bak$', '\~$']
let g:NERDTreeShowBookmarks=1
let g:nerdtree_tabs_focus_on_files=1
let g:NERDTreeMapOpenInTabSilent = '<RightMouse>'
let g:NERDTreeWinSize = 50
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite,.DS_Store,*/node_modules/*
nnoremap <silent> <F2> :NERDTreeFind<CR>
nnoremap <silent> <F3> :NERDTreeToggle<CR>

"" [config] ultisnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsSnippetDirectories=[$HOME."/.config/nvim/UltiSnips"]
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsUsePythonVersion = 3

"" [config] fzf.vim
set wildmode=list:longest,list:full
set wildignore+=*.o,*.obj,.git,*.rbc,*.pyc,__pycache__
let $FZF_DEFAULT_COMMAND =  "find * -path '*/\.*' -prune -o -path 'node_modules/**' -prune -o -path 'target/**' -prune -o -path 'dist/**' -prune -o  -type f -print -o -type l -print 2> /dev/null"

""" ripgrep
if executable('rg')
  let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --follow --glob "!.git/*"'
  set grepprg=rg\ --vimgrep
  command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)
endif

""" nnoremap <silent> <leader><space> :FZF -m<CR>
nnoremap <silent> <Leader>rg :Rg<CR>
nnoremap <silent> <leader>o :Files<CR>
nnoremap <silent> <Leader>. :Tags<CR>
" recovery commands from history through FZF
nmap <leader>y :History:<CR>

" invert selected string: ve,is
vnoremap <silent> <Leader>is :<C-U>let old_reg_a=@a<CR>
 \:let old_reg=@"<CR>
 \gv"ay
 \:let @a=substitute(@a, '.\(.*\)\@=',
 \ '\=@a[strlen(submatch(1))]', 'g')<CR>
 \gvc<C-R>a<Esc>
 \:let @a=old_reg_a<CR>
 \:let @"=old_reg<CR>

"" [config] CoC
nmap <leader>d <Plug>(coc-definition)
nmap <leader>r <Plug>(coc-references)
nmap <leader>i <Plug>(coc-implementation)
nmap <leader>rn <Plug>(coc-rename)
""" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
vmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)
""" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction-cursor)
""" Fix autofix problem of current line
nmap <leader>aq  <Plug>(coc-fix-current)
command! -nargs=0 Format :call CocAction('format')
inoremap <silent><expr> <TAB>
    \ coc#pum#visible() ? coc#pum#next(1) :
    \ CheckBackspace() ? "\<Tab>" :
    \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
inoremap <silent><expr> <leader>. coc#refresh()
" inoremap <silent><expr> <c-space> coc#refresh()
nmap <silent> [l <Plug>(coc-diagnostic-prev)
nmap <silent> ]l <Plug>(coc-diagnostic-next)
nmap <silent> [k :CocPrev<cr>
nmap <silent> ]k :CocNext<cr>
autocmd CursorHold * silent call CocActionAsync('highlight')
vmap <leader>f <Plug>(coc-format-selected)
nmap <leader>f <Plug>(coc-format-selected)
nnoremap <leader>K :call <SID>show_documentation()<CR>
nnoremap <leader>sy  :<C-u>CocList -I symbols<cr>
nnoremap <leader>fu  :<C-u>CocList outline<cr>
" nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>

function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

"*****************************************************************************
"" Mappings
"*****************************************************************************

"" Split
noremap <Leader>h :<C-u>split<CR>
noremap <Leader>v :<C-u>vsplit<CR>

"" Zoom a vim pane, <C-w>= to re-balance
nnoremap <Leader>- :wincmd _<cr>:wincmd \|<cr>
nnoremap <Leader>= :wincmd =<cr>

function! Expand(exp) abort
    let l:result = expand(a:exp)
    return l:result ==# '' ? '' : "file://" . l:result
endfunction

"" Terminal emulation
nnoremap <silent> <leader>sh :terminal<CR>

"" Tabs
noremap <Tab> :tabnext<CR>
noremap <S-Tab> :tabprev<CR>
nnoremap <S-H> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <S-L> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>

"" Set working directory
" nnoremap <leader>. :lcd %:p:h<CR>
" nnoremap <leader>cd :lcd %:p:h<CR>:pwd<CR>

"" Opens an edit command with the path of the currently edited file filled in
noremap <Leader>e :e <C-R>=expand("%:p:h") . "/" <CR>

"" Opens a tab edit command with the path of the currently edited file filled
noremap <Leader>te :tabe <C-R>=expand("%:p:h") . "/" <CR>

"" goimports
"map <f9> <Esc>:%!goimports<CR>
map <f9> <Esc>:GoImports<CR>
map <f10> <Esc>:GoAlternate<CR>
map <f7> <Esc>:GoExtract<CR>


nnoremap <space> za
nnoremap <leader>/ :nohlsearch<CR>
nnoremap <C-A-j> :m .+1<CR>==
nnoremap <C-A-k> :m .-2<CR>==
inoremap <C-A-j> <Esc>:m .+1<CR>==gi
inoremap <C-A-k> <Esc>:m .-2<CR>==gi
vnoremap <C-A-j> :m '>+1<CR>gv=gv
vnoremap <C-A-k> :m '>-2<CR>gv=gv
nnoremap <C-J> m`o<Esc>``
nnoremap <C-K> m`O<Esc>``
map <C-l> 5zl
map <C-h> 5zh

" Disable visualbell
set noerrorbells visualbell t_vb=
if has('autocmd')
  autocmd GUIEnter * set visualbell t_vb=
endif

"" Copy/Paste/Cut
" if has('unnamedplus')
  " set clipboard=unnamed,unnamedplus
" endif
" 
" noremap YY "+y<CR>
" noremap <leader>p "+gP<CR>
" noremap XX "+x<CR>

if has('macunix')
  " pbcopy for OSX copy/paste
  vmap <C-x> :!pbcopy<CR>
  vmap <C-c> :w !pbcopy<CR><CR>
endif

"" Buffer nav
noremap <leader>z :bp<CR>
noremap <leader>x :bn<CR>
nnoremap <silent> <leader>b :Buffers<CR>
noremap <leader>c :bd<CR>


"" Switching windows
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
noremap <C-h> <C-w>h

"" Vmap for maintain Visual Mode after shifting > and <
vmap < <gv
vmap > >gv

"" Move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Abbreviations
abbr clg console.log('')<esc>2ha


"*****************************************************************************
"" Autocmd Rules
"*****************************************************************************
"" The PC is fast enough, do syntax highlight syncing from start unless 200 lines
augroup vimrc-sync-fromstart
  autocmd!
  autocmd BufEnter * :syntax sync maxlines=200
augroup END

"" Remember cursor position
augroup vimrc-remember-cursor-position
  autocmd!
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
augroup END

set autoread
autocmd Filetype yaml setlocal ts=2 sw=2 sts=2 expandtab
au BufNewFile,BufRead Jenkins*le* setf groovy


"*****************************************************************************
"" Commands
"*****************************************************************************
" remove trailing whitespaces
command! FixWhitespace :%s/\s\+$//e

function ReplaceAll()
  let a = expand("<cword>")
  let b = input('Replace "' . a . '" by: ')
  if !empty(b)
    execute '%Subvert/' . a . '/' . b . '/g'
  endif
endfunction

noremap <Leader>ra :call ReplaceAll()<CR>


"*****************************************************************************
"" Go
"*****************************************************************************
autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4 softtabstop=4
let g:go_imports_autosave = 0
let g:go_fmt_autosave = 0


"*****************************************************************************
"" Javascript
"*****************************************************************************
:call extend(g:polyglot_disabled, ['css'])
autocmd BufRead,BufNewFile *.vue setlocal filetype=vue  " .html.javascript.css
autocmd Filetype vue setlocal iskeyword+=- ts=2 sw=2 expandtab
au FileType vue let b:coc_root_patterns = ['.git', '.env', 'package.json', 'tsconfig.json', 'jsconfig.json', 'vite.config.ts', 'vite.config.js', 'vue.config.js', 'nuxt.config.ts']
let g:LanguageClient_serverCommands = {'vue': ['vls']}

" html
autocmd Filetype html setlocal ts=2 sw=2 expandtab
autocmd Filetype javascript UltiSnipsAddFiletypes nodejs.javascript

" javascript
let g:javascript_enable_domhtmlcss = 1

" vim-javascript
augroup vimrc-javascript
  autocmd!
  autocmd FileType javascript setl tabstop=2 shiftwidth=2 expandtab softtabstop=2
augroup END


"*****************************************************************************
"" Python
"*****************************************************************************
augroup vimrc-python
  autocmd!
  autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8 colorcolumn=140
      \ formatoptions+=croq softtabstop=4
      \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
augroup END

let python_highlight_all = 1
" jedi-vim
" let g:jedi#popup_on_dot = 0
" let g:jedi#goto_assignments_command = "<leader>g"
" let g:jedi#goto_definitions_command = "<leader>d"
" let g:jedi#documentation_command = "K"
" let g:jedi#usages_command = "<leader>n"
" let g:jedi#rename_command = "<leader>r"
" let g:jedi#show_call_signatures = "0"
" let g:jedi#completions_command = "<C-Space>"
" let g:jedi#smart_auto_mappings = 0

"*****************************************************************************
"" Clojure
"*****************************************************************************
let g:sexp_enable_insert_mode_mappings = 1
let g:clojure_fuzzy_indent_patterns = ['^doto', '^with', '^def', '^let', 'go-loop', 'match', '^context', '^GET', '^PUT', '^POST', '^PATCH', '^DELETE', '^ANY', 'this-as', '^are', '^dofor']
let g:clojure_align_multiline_strings = 1
let g:clojure_maxlines = 100
let g:clj_refactor_prefix_rewriting=0
nmap <Leader>F <Plug>FireplacePrint<Plug>(sexp_outer_top_list)``
nmap <Leader>f <Plug>FireplacePrint<Plug>(sexp_outer_list)``
nmap <Leader>d [<C-D>
nmap <Leader>E :%Eval<CR>
nmap <Leader>R cqp(require 'clojure.tools.namespace.repl) (clojure.tools.namespace.repl/refresh)<CR>
nmap <Leader>w( ysie)
nmap <Leader>w[ ysie]
nmap <Leader>w{ ysie}
nmap <Leader>w" ysie"
vmap <Leader>w( S)
vmap <Leader>w[ S]
vmap <Leader>w{ S}
vmap <Leader>w" S"

nmap <S-Right> <Plug>(sexp_capture_next_element)<Plug>(sexp_indent)
nmap <S-Left> <Plug>(sexp_emit_tail_element)<Plug>(sexp_indent)
imap <S-Right> <C-O><Plug>(sexp_capture_next_element)<C-O><Plug>(sexp_indent)
imap <S-Left> <C-O><Plug>(sexp_emit_tail_element)<C-O><Plug>(sexp_indent)

let g:sexp_mappings = {
      \ 'sexp_outer_list':                'af',
      \ 'sexp_inner_list':                'if',
      \ 'sexp_outer_top_list':            'aF',
      \ 'sexp_inner_top_list':            'iF',
      \ 'sexp_outer_string':              'as',
      \ 'sexp_inner_string':              'is',
      \ 'sexp_outer_element':             'ae',
      \ 'sexp_inner_element':             'ie',
      \ 'sexp_move_to_prev_bracket':      '(',
      \ 'sexp_move_to_next_bracket':      ')',
      \ 'sexp_indent_top':                '=-',
      \ 'sexp_round_head_wrap_element':   '<Leader>W',
      \ 'sexp_swap_element_backward':     '<Leader>C',
      \ 'sexp_swap_element_forward':      '<Leader>c',
      \ 'sexp_raise_element':             '<Leader>''',
      \ 'sexp_emit_head_element':         '<Leader>{',
      \ 'sexp_emit_tail_element':         '<Leader>}',
      \ 'sexp_capture_prev_element':      '<Leader>[',
      \ 'sexp_capture_next_element':      '<Leader>]',
      \ 'sexp_flow_to_next_open_bracket': '<M-f>',
      \ 'sexp_flow_to_prev_open_bracket': '<M-b>',
      \ } 

nnoremap <silent> crcc :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'cycle-coll', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> crth :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'thread-first', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> crtt :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'thread-last', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> crtf :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'thread-first-all', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> crtl :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'thread-last-all', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> cruw :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'unwind-thread', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> crua :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'unwind-all', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> crml :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'move-to-let', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1, input('Binding name: ')]})<CR>
nnoremap <silent> cril :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'introduce-let', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1, input('Binding name: ')]})<CR>
nnoremap <silent> crel :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'expand-let', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> cram :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'add-missing-libspec', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> crcn :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'clean-ns', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> crcp :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'cycle-privacy', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> cris :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'inline-symbol', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> cref :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'extract-function', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1, input('Function name: ')]})<CR>
nnoremap <silent> crci :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'cursor-info', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> crsi :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'server-info', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> <leader>T :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'move-coll-entry-up', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> <leader>t :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'move-coll-entry-down', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>
nnoremap <silent> crmf :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'move-form', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1, input("File: ", "", "file")]})<CR>
nnoremap <silent> crfe :call CocRequest('clojure-lsp', 'workspace/executeCommand', {'command': 'create-function', 'arguments': [Expand('%:p'), line('.') - 1, col('.') - 1]})<CR>

" Copies the log-path to your clipboard
nnoremap <silent> crsl :call setreg('*', CocRequest('clojure-lsp', 'clojure/serverInfo/raw')['log-path'])<CR>
" Connects to nrepl
nnoremap <silent> crsp :execute 'Connect' CocRequest('clojure-lsp', 'clojure/serverInfo/raw')['port']<CR>


function! s:LoadClojureContent(uri)
  setfiletype clojure
  let content = CocRequest('clojure-lsp', 'clojure/dependencyContents', {'uri': a:uri})
  call setline(1, split(content, "\n"))
  setl nomodified
  setl readonly
endfunction
autocmd BufReadCmd,FileReadCmd,SourceCmd jar:file://* call s:LoadClojureContent(expand("<amatch>"))

highlight Normal guibg=#101010 guifg=white
highlight CursorColumn guibg=#202020
highlight Keyword guifg=#FFAB52
highlight CursorLine guibg=#202020
hi clear CocHighlightText
hi CocHighlightText guibg=#472004
hi CocFadeOut gui=undercurl cterm=undercurl

