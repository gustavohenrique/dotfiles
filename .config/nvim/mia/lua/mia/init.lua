-- package.path = package.path .. ";/Users/gustavo/.config/nvim/lua/resty-http/lib/?.lua"
local M = {}
local plenary = require("plenary.curl")

local function get_api_url()
    return vim.g.mia_api_url or ''
end

local function send_to_api(text, prompt)
    local ft = vim.bo.filetype or "programming"
    local intro = "you are a software engineer expert in " .. ft .. ". You need to provide me with implementations of functionalities that I request. Always respond to me only with code, never with greetings or explanatory messages."
    local p = prompt or ""
    local t = text or ""
    local full_prompt = intro .. "\n" .. p .. "\n\n" .. t
    local json_body = vim.fn.json_encode({
        model = vim.g.mia_model or "llama3.2",
        prompt = full_prompt,
        stream = false
    })
    
    local response_buffer = ""
    local res = plenary.post(get_api_url(), {
        body = json_body,
        headers = {
            ["Content-Type"] = "application/json",
        },
        timeout = 30000 -- 30 seconds timeout
        -- stream = function(chunk)
        --     local ok, result = pcall(vim.fn.json_decode, chunk)
        --     if ok and result and result.response then
        --         response_buffer = response_buffer .. result.data.text
        --     end
        -- end
    })
    
    if not res or res.status ~= 200 then
        local error_details = "API request failed: " .. (res.error or "Unknown error")
        if res and res.body then
            error_details = error_details .. "\nResponse content: " .. vim.inspect(res.body)
        end
        vim.notify(error_details, vim.log.levels.ERROR)
        return nil
    end

		local ok, result = pcall(vim.fn.json_decode, res.body)
    if ok and result and result.data then
        return result.data.text
    end
		vim.notify("API request failed: Invalid response format", vim.log.levels.ERROR)
		return nil
    
    -- if response_buffer ~= "" then
    --     return response_buffer
    -- else
    --     vim.notify("API request failed: No response received", vim.log.levels.ERROR)
    --     return nil
    -- end
end

function M.process_text(range, prompt)
    local start_line, end_line
    local mode = vim.fn.mode()
    
    if range then
        start_line = range[1]
        end_line = range[2]
    elseif mode == "v" or mode == "V" or mode == "" then
        start_line = vim.fn.line("v")
        end_line = vim.fn.line(".")
        
        if start_line > end_line then
            start_line, end_line = end_line, start_line
        end
    else
        start_line = 1
        end_line = vim.fn.line("$")
    end
    
    local lines = vim.api.nvim_buf_get_lines(0, start_line - 1, end_line, false)
    local text = table.concat(lines, "\n")
    
    local processed_text = send_to_api(text, prompt)
    if not processed_text then return end
    
    vim.api.nvim_buf_set_lines(0, start_line - 1, end_line, false, vim.split(processed_text, "\n"))
end

function M.setup()
    vim.api.nvim_create_user_command("Mia", function(opts)
        local prompt = vim.fn.input("Enter prompt: ")
        M.process_text({opts.line1, opts.line2}, prompt)
    end, { range = true })
    
    -- vim.keymap.set("n", "<leader>sa", function()
    --     local prompt = vim.fn.input("Enter prompt: ")
    --     M.process_text(nil, prompt)
    -- end, { noremap = true, silent = true })
    
    -- vim.keymap.set("v", "<leader>sa", function()
    --     local prompt = vim.fn.input("Enter prompt: ")
    --     M.process_text(nil, prompt)
    -- end, { noremap = true, silent = true })
end

return M

