import http.server
import socketserver
from http import HTTPStatus


class Handler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(HTTPStatus.OK)
        self.end_headers()
        self.wfile.write(b'Python server in 9002')


if __name__ == '__main__':
    httpd = socketserver.TCPServer(('', 9002), Handler)
    print('Listening on 9002')
    httpd.serve_forever()
