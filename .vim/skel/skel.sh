#!/bin/sh

function main {
   echo "Hi! $1"
}

case "$1" in
    run) main $2 ;;
    *)
        echo "Usage ./$0 run"
        ;;
esac
