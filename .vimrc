" call plug#begin('~/.vim/plugged')
" if empty(glob('~/.vim/autoload/plug.vim'))
    " silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
                " \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    " autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
" endif
" filetype plugin indent on
" 
" Plug 'sheerun/vim-polyglot'            " A solid language pack for Vim
" Plug 'preservim/nerdcommenter'         " Comment functions so powerful
" 
" Themes
" Plug 'junegunn/seoul256.vim'
" Plug 'sjl/badwolf'
" 
" call plug#end()

"*****************************************************************************
"" Look and Feel
"*****************************************************************************"
syntax on                   " syntax highlighting
set number                  " add line numbers
set ruler
set gcr=a:blinkon0
set scrolloff=3
set laststatus=2
set title
set titleold="Vim"
set titlestring=%F

"" Themes
let g:seoul256_background = 233
let g:gruvbox_contrast_dark = "hard"
let g:badwolf_tabline = 3
let g:badwolf_css_props_highlight = 1
silent! colorscheme badwolf

"" IndentLine
let g:indentLine_enabled = 1
let g:indentLine_concealcursor = 0
let g:indentLine_char = '┆'
let g:indentLine_faster = 1


"*****************************************************************************
"" Basic Setup
"*****************************************************************************"

"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set fileformats=unix,dos,mac

"" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase

set nocompatible            " disable compatibility to old-time vi
set showmatch               " show matching brackets.
set ignorecase              " case insensitive matching
set mouse=a                 " middle-click paste with mouse
set hlsearch                " highlight search results
set tabstop=4               " number of columns occupied by a tab character
set softtabstop=4           " see multiple spaces as tabstops so <BS> does the right thing
set expandtab               " converts tabs to white space
set shiftwidth=4            " width for autoindents
set autoindent              " indent a new line the same amount as the line just typed
set wildmode=longest,list   " get bash-like tab completions
" set cc=120                " set an 80 column border for good coding style
set lazyredraw              " redraw only when we need to.
set backspace=indent,eol,start
set noswapfile
set nowrap
set cursorline
set wildmenu
set wildignorecase
set foldenable
set foldlevelstart=9
set foldnestmax=10
set foldmethod=indent
set hidden
set nobackup
set nowritebackup

let g:mapleader = ','
let g:tablineclosebutton=1


"" Comments
let g:NERDSpaceDelims = 1
let g:NERDCommentEmptyLines = 1
let g:NERDCompactSexyComs = 1
let g:NERDCustomDelimiters = {
  \ 'vue': { 'left': '//', 'leftAlt': '/*','rightAlt': '*/' },
  \ 'html': { 'left': '//', 'leftAlt': '<!--','rightAlt': '-->' },
  \ 'javascript': { 'left': '//', 'leftAlt': '/*', 'right': '*/' }
\}


"*****************************************************************************
"" Mappings
"*****************************************************************************

"" Split
noremap <Leader>h :<C-u>split<CR>
noremap <Leader>v :<C-u>vsplit<CR>

"" Zoom a vim pane, <C-w>= to re-balance
nnoremap <Leader>- :wincmd _<cr>:wincmd \|<cr>
nnoremap <Leader>= :wincmd =<cr>

"" Terminal emulation
nnoremap <silent> <leader>sh :terminal<CR>

"" Tabs
nnoremap <Tab> :tabnext<CR>
nnoremap <S-Tab> :tabprev<CR>
nnoremap <S-H> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <S-L> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>

"" Set working directory
nnoremap <leader>. :lcd %:p:h<CR>
" nnoremap <leader>cd :lcd %:p:h<CR>:pwd<CR>

"" Opens an edit command with the path of the currently edited file filled in
noremap <Leader>e :e <C-R>=expand("%:p:h") . "/" <CR>

"" Opens a tab edit command with the path of the currently edited file filled
noremap <Leader>te :tabe <C-R>=expand("%:p:h") . "/" <CR>

"" goimports
map <f9> <Esc>:%!goimports<CR>

nnoremap <space> za
nnoremap <leader>s :mksession<CR> 
nnoremap <leader>/ :nohlsearch<CR>
nnoremap <leader>u :GundoToggle<CR>
nnoremap <C-A-j> :m .+1<CR>==
nnoremap <C-A-k> :m .-2<CR>==
inoremap <C-A-j> <Esc>:m .+1<CR>==gi
inoremap <C-A-k> <Esc>:m .-2<CR>==gi
vnoremap <C-A-j> :m '>+1<CR>gv=gv
vnoremap <C-A-k> :m '>-2<CR>gv=gv
nnoremap <C-J> m`o<Esc>``
nnoremap <C-K> m`O<Esc>``
map <C-l> 5zl
map <C-h> 5zh

" Alternative to NERDTree
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
function! ToggleVExplorer()
  if exists("t:expl_buf_num")
    let expl_win_num = bufwinnr(t:expl_buf_num)
    if expl_win_num != -1
      let cur_win_nr = winnr()
      exec expl_win_num . 'wincmd w'
      close
      exec cur_win_nr . 'wincmd w'
      unlet t:expl_buf_num
    else
      unlet t:expl_buf_num
    endif
  else
    exec '1wincmd w'
    Vexplore
    let t:expl_buf_num = bufnr("%")
  endif
endfunction
noremap <silent> <f3> :call ToggleVExplorer()<CR>

" set runtimepath^=~/.vim/bundle/command-t
" set runtimepath^=~/.vim/bundle/calendar.vim
set runtimepath^=~/.vim/plugin/abolish.vim

"" ctrlp
set runtimepath^=~/.vim/plugin/ctrlp.vim
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git|node_modules|venv|target'
let g:ctrlp_working_path_mode = 'c'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_map = ''
nnoremap <leader><space> :CtrlP<CR>
" let g:ctrlp_match_window = 'bottom,order:ttb'
" let g:ctrlp_switch_buffer = 0
" let g:ctrlp_user_command = 'find %s -type f'

" Disable visualbell
set noerrorbells visualbell t_vb=
if has('autocmd')
  autocmd GUIEnter * set visualbell t_vb=
endif

"" Copy/Paste/Cut
" if has('unnamedplus')
  " set clipboard=unnamed,unnamedplus
" endif
" 
" noremap YY "+y<CR>
" noremap <leader>p "+gP<CR>
" noremap XX "+x<CR>

if has('macunix')
  " pbcopy for OSX copy/paste
  vmap <C-x> :!pbcopy<CR>
  vmap <C-c> :w !pbcopy<CR><CR>
endif

"" Buffer nav
noremap <leader>z :bp<CR>
noremap <leader>x :bn<CR>
nnoremap <silent> <leader>b :Buffers<CR>
"" Close buffer
noremap <leader>c :bd<CR>

"" Switching windows
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
noremap <C-h> <C-w>h

"" Vmap for maintain Visual Mode after shifting > and <
vmap < <gv
vmap > >gv

"" Move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Abbreviations
abbr clg console.log('')<esc>2ha


"*****************************************************************************
"" Autocmd Rules
"*****************************************************************************
"" Remember cursor position
augroup vimrc-remember-cursor-position
  autocmd!
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
augroup END

set autoread
autocmd Filetype yaml setlocal ts=2 sw=2 sts=2 expandtab


"*****************************************************************************
"" Commands
"*****************************************************************************
" remove trailing whitespaces
command! FixWhitespace :%s/\s\+$//e


"*****************************************************************************
"" Go
"*****************************************************************************
autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4 softtabstop=4


"*****************************************************************************
"" Frontend
"*****************************************************************************

" html
autocmd Filetype html setlocal ts=2 sw=2 expandtab
autocmd Filetype vue setlocal ts=2 sw=2 expandtab

" javascript
let g:javascript_enable_domhtmlcss = 1

" vim-javascript
augroup vimrc-javascript
  autocmd!
  autocmd FileType javascript setl tabstop=2|setl shiftwidth=2|setl expandtab softtabstop=2
augroup END


"*****************************************************************************
"" Python
"*****************************************************************************
augroup vimrc-python
  autocmd!
  autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8 colorcolumn=79
      \ formatoptions+=croq softtabstop=4
      \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
augroup END

augroup templates
  autocmd!
  autocmd BufNewFile * silent!
      \ 0r ~/.vim/skel/skel.%:e|norm G


" custom
function ReplaceAll()
  let a = expand("<cword>")
  let b = input('Replace "' . a . '" by: ')
  if !empty(b)
    execute '%Subvert/' . a . '/' . b . '/g'
  endif
endfunction

noremap <Leader>r :call ReplaceAll()<CR>
