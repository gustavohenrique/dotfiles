local wezterm = require 'wezterm'
local config = wezterm.config_builder()
local mux = wezterm.mux

-- This is where you actually apply your config choices

-- For example, changing the color scheme:
-- config.color_scheme = 'AdventureTime'
-- config.font = wezterm.font 'JetBrains Mono'
config.color_scheme = 'Batman'
config.font = wezterm.font 'JetBrains Mono'
config.font_size = 14.0
config.font_dirs = {"fonts"}
config.font_locator = "ConfigDirsOnly"
config.default_prog = { 'powershell' }
config.front_end = "OpenGL"

config.use_fancy_tab_bar = false
config.show_tabs_in_tab_bar = true
config.show_new_tab_button_in_tab_bar = true



wezterm.on("gui-startup", function()
  local tab, pane, window = mux.spawn_window{}
  window:gui_window():maximize()
end)

-- and finally, return the configuration to wezterm
return config