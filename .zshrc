# Path to your oh-my-zsh installation.
DOTFILES=$HOME/Documents/dotfiles
export ZSH=$DOTFILES/shell/ohmyzsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="intheloop"

if [ "${container}" = "lxc" ]; then
  ZSH_THEME="apple"
fi

if [ -f /proc/sys/fs/binfmt_misc/WSLInterop ]; then
  ZSH_THEME="dst"
fi

if [ $(uname -s) = "Linux" ]; then
  # Manjaro
  ZSH_THEME="gentoo"
fi

#ZSH_THEME="steeef"
#ZSH_THEME="robbyrussell"
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

ZSH_AUTOSUGGEST_USE_ASYNC="true"

plugins=(
  colorize
  git
  dotenv
  macos
  pip
  python
  pyenv
  kubectl
  history-substring-search
)

source $ZSH/oh-my-zsh.sh
source $DOTFILES/shell/exports.sh
source $DOTFILES/shell/aliases.sh
source $DOTFILES/shell/functions.sh


[ -f "/usr/bin/thefuck" ] && eval "$(thefuck --alias)"
[ -f "$HOME/.gcloud_completion.zsh.inc" ] && source $HOME/.gcloud_completion.zsh.inc
[ -f "$DOTFILES/shell/goto.sh" ] && source $DOTFILES/shell/goto.sh
[ -f ~/.tnsrc ] && source ~/.tnsrc
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh


# Fix tilix on GTK3 Wayland
if [ ! -z ${TILIX_ID+x} ]; then
    source /etc/profile.d/vte.sh
fi

