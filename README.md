## Getting Started

```sh
sudo pacman -S zsh  # or sudo apt install zsh
curl -sL http://dotfiles.gustavohenrique.com | bash
sudo chsh `whoami` -s `which zsh`
tmux  # press ctrl + I to install plugins
```

## Post installation

### Desktop

- Opera
- Chrome
- Firefox
- Gimp
- Sublime Text
- VSCode
- zsh
- Vlc
- Transmission
- Tillix
- Slack
- Discord
- Skype
- Zoom

### OS

- [MacOS](https://gist.github.com/gustavohenrique/9e6b9bd206783fc010b9)
- [Mint](https://gist.github.com/gustavohenrique/c273680c1d69d73cbab388e958c49e37)
- [ElementaryOS](https://gist.github.com/gustavohenrique/7251097)

### Programming Languages

- [Go](https://golang.org/dl/)
- [PyEnv](https://gist.github.com/gustavohenrique/d694fa7b77ec8176c52903fb19f6b4bd)
- Rust `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
- NVM `git clone https://github.com/nvm-sh/nvm.git ~/.nvm`
- Rbenv
```bash
    git clone https://github.com/rbenv/rbenv.git ~/.rbenv
    mkdir -p "$(rbenv root)"/plugins
    git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build
```
- Flutter
```bash
install jdk17-openjdk
tar xf flutter_linux_3.24.3-stable.tar.xz -C ~/Applications
flutter --disable-analytics
unzip cmdline-tools.zip
mkdir -p ~/Android/Sdk/cmdline-tools
mv cmdline-tools ~/Android/Sdk/cmdline-tools/latest
sdkmanager "platforms;android-33"
sdkmanager "emulator" "build-tools;33.0.0" "cmake;3.22.1"
```

### Developer Tools

- Qemu/Virt Manager
- Caddy
- Hugo
- Gopls
- Ngrok
- Tor Browser

### Vim/Neovim/Lvim

https://www.lunarvim.org/docs/installation


```sh
# Language servers
go install golang.org/x/tools/gopls@latest
open https://github.com/clojure-lsp/clojure-lsp/releases

# increase pip speed
sudo sysctl net.ipv6.conf.all.disable_ipv6=1

# pip from pyenv python install
pip install msgpack
pip install pynvim
pip install jedi

<pkg manager> install ripgrep
```

## OSX

### Apps

- iTerm (and colors schemas). Set font Inconsolata-g for Powerline because tmux
- HomeBrew
- Via App Store: UnRAR, Slack, Kindle, iSee Lite, Evernote, PDF Reader Pro, Foxit Reader

### Fonts

Open `Font Book.app`, click in `+` and select all fonts.

## Linux Commands

https://wiki.archlinux.org/index.php/Pacman/Rosetta

### Arch

```sh
pacman -S <name>      # install
pacman -Rs <name>     # remove
pacman -Ss <name>     # search string in name and description
pacman -Syu           # update and upgrage
pacman -Sc            # clean
pacman -Sw            # download only
pacman -Qi <name>     # show package info
pacman -Ql <name>     # package content
pacman -Qo <filepath> # package which provides file
pacman -Qc <name>     # package's changelog

vim /etc/pacman.conf  # add other repos
```

## Linux Config

- Keyboard Layout US Int
- Inconsolata for Powerline in Terminal

Cedilla

```bash
cat /etc/environment
GTK_IM_MODULE=cedilla
QT_IM_MODULE=cedilla
sed -i 's/Ć/Ç/g' /usr/share/X11/locale/en_US.UTF-8/Compose
sed -i 's/ć/ç/g' /usr/share/X11/locale/en_US.UTF-8/Compose
```

## Terminal

## Fonts

```bash
fc-cache -fv
```

### Tilix

```bash
# dconf dump /com/gexperts/Tilix/ > tilix.dconf
dconf load /com/gexperts/Tilix/ < tilix.dconf
```

## LXD

https://gist.github.com/gustavohenrique/4a7e99696bc85030d88803dc58e86c00

## AWS SDK

```bash
cd `mktemp -d`
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip *.zip
cd aws
./install -i ~/.local/aws-cli -b ~/.local/bin
```

## Opera & H264 video codec

```bash
URL=$(curl -Ls -o /dev/null -w %{url_effective} https://github.com/nwjs-ffmpeg-prebuilt/nwjs-ffmpeg-prebuilt/releases/latest)
FFMPEGVER=${URL%\"*}
FFMPEGVER=${FFMPEGVER##*/}
FFMPEGZIP=${FFMPEGVER}-linux-x64.zip
curl -sL -o libffmpeg.zip "https://github.com/iteufel/nwjs-ffmpeg-prebuilt/releases/download/${FFMPEGVER}/${FFMPEGZIP}"
unzip libffmpeg.zip
sudo mv /usr/lib/x86_64-linux-gnu/opera/libffmpeg.so /usr/lib/x86_64-linux-gnu/opera/old-libffmpeg.so 
sudo mv libffmpeg.so /usr/lib/x86_64-linux-gnu/opera/
```

## Secrets Encrypted Dir

```bash
cd ~/Documents
dd if=/dev/zero of=secrets.img bs=1M count=512
sudo losetup /dev/loop1 $PWD/secrets.img  # loop0 is used by LXD
sudo cryptsetup -q luksFormat -y /dev/loop1
# Insert a password

secretson   # mount secrets
secretsoff  # unmount
```

## Disable lock out user

```bash
echo "deny = 0" | sudo tee -a /etc/security/faillock.conf
```

## Increase file descriptors limits

```bash
cat <<EOF | sudo tee -a /etc/security/limits.conf
# Increase open file limits
*    soft    nofile  8092
*    hard    nofile  65535

# Increase process limits
*    soft    nproc   4096
*    hard    nproc   16384

# Memory lock limits 8 GB in KB
*    soft    memlock 8388608
*    hard    memlock 8388608
EOF

## Adjust Sysctl

```bash
# https://wiki.archlinux.org/title/Sysctl

# for quic protocol
sudo sysctl -w net.core.rmem_max=7500000
sudo sysctl -w net.core.wmem_max=7500000
```
```

## go with private repos

```sh
GIT_SSH_COMMAND="ssh -i ~/.ssh/some.key" go mod tidy
```
