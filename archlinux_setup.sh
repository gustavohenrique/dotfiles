#!/bin/zsh

if [ ! -d "$HOME/.local/aws-cli" ]; then
  cd `mktemp -d`
  curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
  unzip *.zip
  cd aws
  ./install -i ~/.local/aws-cli -b ~/.local/bin
  cd -
fi

alias install='sudo pacman -Syu'
install alacritty \
  base-devel tk fuse2fs jdk17-openjdk clang cmake ninja \
  fzf ripgrep bat \
  docker docker-compose \
  tmux xsel powerline-fonts \
  gimp inkscape shotwell \
  brave-browser chromium firefox firefox-developer-edition

alias flat='flatpak install -y'
flat flathub com.opera.Opera
flat flathub com.microsoft.Edge

if [ ! -d "$HOME/.rbenv" ]; then
  git clone https://github.com/rbenv/rbenv.git ~/.rbenv
  mkdir -p "$(rbenv root)"/plugins
  git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build
  version="3.3.6"
  rbenv install $version
  rbenv global $version
fi

if [ ! -d "$HOME/.pyenv" ]; then
  git clone https://github.com/pyenv/pyenv.git ~/.pyenv
  source ~/.zshrc
  git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
  version="3.12"
  pyenv install ${version}
fi

if [ ! -d "$HOME/.nvm" ]; then
  git clone https://github.com/nvm-sh/nvm.git ~/.nvm
  source ~/.zshrc
  nvm install v22
  npm i -g npm
  corepack enable
fi

if [ ! -d "$APPLICATIONS_DIR/go" ]; then
  version="1.23.4"
  cd ~/Downloads
  curl -sL https://go.dev/dl/go${version}.linux-amd64.tar.gz -o go.tar.gz
  tar zxf go.tar.gz
  mv go "${APPLICATIONS_DIR}/go${version}"
  ln -sn "${APPLICATIONS_DIR}/go${version}" "${APPLICATIONS_DIR}/go"
  source ~/.zshrc
  go install golang.org/x/tools/gopls@latest
  go install github.com/fatih/gomodifytags@latest
  go install github.com/fatih/motion@latest
  go install github.com/jstemmer/gotags@latest
fi

# Mount ntfs writeable as normal user
sudo addgroup ntfsuser   
sudo chown root:ntfsuser $(which ntfs-3g)  
sudo chmod 4750 $(which ntfs-3g)  
sudo usermod -aG ntfsuser gustavo
sudo mkdir /mnt/extra
sudo chmod 0755 /mnt/extra
## Add in /etc/fstab. See UUID using gparted
UUID=C8187DEC187DDA40 /mnt/extra ntfs-3g uid=1000,gid=1000,umaks=0022 0 0

