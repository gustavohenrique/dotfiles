#!/bin/sh
[ ! -d "$HOME/.config" ] && mkdir ~/.config
cp .config/*.* ~/.config/

ln -snf $PWD/.config/lvim ~/.config/lvim
ln -snf $PWD/.config/nvim ~/.config/nvim
ln -snf $PWD/.tmux ~/
ln -snf $PWD/.tmux.conf ~/
ln -snf $PWD/.vim ~/
ln -snf $PWD/.vimrc ~/
ln -snf $PWD/.gitconfig ~/
ln -snf $PWD/.zshrc ~/
ln -snf $PWD/.psqlrc ~/

if [ $(uname -s) = "Darwin" ]; then
    cp -rf $PWD/.fonts/* ~/Library/Fonts/
else
    ln -snf $PWD/.fonts ~/
    fc-cache -fv
fi

