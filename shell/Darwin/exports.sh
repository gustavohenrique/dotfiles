export GREP_OPTIONS="--color=auto"
export CLICOLOR="auto"
export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx
export HOMEBREW_NO_ANALYTICS=1
export PATH=/opt/homebrew/bin:${PATH}
export PATH=/opt/homebrew/opt/java11/bin:${PATH}
export PATH=${PATH}:${HOME}/apps/aws-cli
export GPG_TTY=$(tty)
if command -v multipass >/dev/null 2>&1; then
    export DOCKER_HOST=ssh://ubuntu@dockermultipass # Remember to add docker vm multipass IP in /etc/hosts
fi
eval "$(brew shellenv)"
