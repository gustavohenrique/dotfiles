function free {
    if [ "$1" != "" ]; then
        ps -amcwwwxo "command %mem %cpu" | grep -i "$1" | head -50
    else
        ps -amcwwwxo "command %mem %cpu" | head -20
    fi
}

function realpath {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
