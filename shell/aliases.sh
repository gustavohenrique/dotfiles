alias ls="ls -shFx"
alias ll="ls -la"
alias ns='netstat -natp'

alias compose='docker-compose up -d'
alias dcompose='docker-compose down -v'
alias drm='docker rm -f $(docker ps -a -q)'
alias dps='docker ps -a'
alias dpi='docker images'
alias drmnone="docker images | grep '^<none' | awk '{print $3}' | xargs docker rmi -f"
#alias cp='rsync -ah --progress'

alias act="source env/bin/activate"
alias 2cd='cd ../..'
alias 3cd='cd ../../..'

alias anroidemulator='emulator -list-avds | head -n 1 | xargs emulator -avd'
alias galaxy="${ANDROID_HOME}/emulator/emulator @galaxy"
alias gno='XDG_SESSION_TYPE=wayland dbus-run-session gnome-session'

alias postwoman='docker rm -f postwoman 2>/dev/null; docker run -d --name postwoman -p 9999:3000 liyasthomas/postwoman'
alias flushdns='sudo killall -HUP mDNSResponder; say dns cleared successfully'

alias ubuntu='multipass shell'
alias dfimage="docker run -v /var/run/docker.sock:/var/run/docker.sock --rm chenzj/dfimage"
alias clj='clojure -Sdeps "{:deps {com.bhauman/rebel-readline {:mvn/version \"0.1.4\"} $@}}" -m rebel-readline.main'

alias myphotos="python ${HOME}/Documents/dotfiles/photo_organize.py"

alias gitp='git pull origin $(git rev-parse --abbrev-ref HEAD)'
alias gitpush='git push origin $(git rev-parse --abbrev-ref HEAD)'

alias meminfo="sudo dmidecode --type 17"

# if not MacOS
if [ ! $(uname -s) = "Darwin" ]; then
    alias pbcopy='xsel --clipboard --input'
    alias pbpaste='xsel --clipboard --output'
    alias ls="ls -GshFx --color"
    alias ubuntu='lxc exec ubuntu -- bash -c "su -l ubuntu"'
    alias install='sudo pacman -S $@'
    alias search='pacman -Ss $@'
    alias remove='sudo pacman -Rs $@'
    alias update='sudo pacman -Sy archlinux-keyring && sudo pacman -Sy'
    alias upgrade='sudo pacman -Syu'
    alias package='pacman -Qo $@'
    alias flushdns='sudo systemctl restart dnsmasq && sudo systemctl restart nscd && sudo systemctl restart named'
    alias disablebroadcastmessages='sudo busctl set-property org.freedesktop.login1 /org/freedesktop/login1 org.freedesktop.login1.Manager EnableWallMessages b false'
    distro=`grep -i 'ID=.*' /etc/lsb-release | awk -F '=' '{print $2}' | tr '[:upper:]' '[:lower:]'`
    if [ "$distro" = "ubuntu" ] ||
       [ "$distro" = "linuxmint" ] ||
       [ "$distro" = "pop" ]; then
        alias install='sudo apt-get install -y $@'
        alias search='apt search $@'
        alias remove='sudo apt remove $@'
        alias update='sudo apt update'
        alias upgrade='sudo apt upgrade -y'
        alias package='dpkg -S $@'
    fi
fi
