. `brew --repository`/Library/Contributions/brew_bash_completion.sh

MYBASH_DIR=~/Dropbox/conf/.mybash
. $MYBASH_DIR/aliases.sh
. $MYBASH_DIR/exports.sh
. $MYBASH_DIR/functions.sh
. $MYBASH_DIR/git-prompt.sh
#. $MYBASH_DIR/promptastic.sh

#function _update_ps1() { export PS1="$(/Users/gustavo/apps/promptastic/promptastic.py $?)"; }
#export PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"

