# Configure history for each tmux session
if [[ -n "$TMUX" ]]; then
    export HISTFILE=~/.history_$(tmux display -p '#S')
    export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"
fi

# Avoid duplicates in your history:
export HISTIGNORE="&:ls:ls *:[bf]g:exit"
export GREP_COLOR="4;33"
export CLICOLOR="auto"
export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx
export PGHOST=localhost
export EDITOR=vim
export VISUAL=$EDITOR # Add in /etc/sudoers.d/systemctl Defaults env_keep += "EDITOR"
export SVN_EDITOR=$EDITOR
export SYSTEMD_EDITOR=$EDITOR

# Add no /etc/environment para teclado us-int
export GTK_IM_MODULE=cedilla
export QT_IM_MODULE=cedilla
#export QT_IM_MODULE=xim
# Dont poll for networks every 30
export QT_BEARER_POLL_TIMEOUT=-1

# cat > ~/.XCompose <<EOF
# include "%S/en_US.UTF-8/Compose"
# <dead_acute> <C>                        : "Ç"
# <dead_acute> <c>                        : "ç"
# EOF

export APPLICATIONS_DIR=$HOME/Applications
export DART_SDK="/opt/dart-sdk/bin"

if [ $(uname -s) = "Darwin" ]; then
    export APPLICATIONS_DIR=$HOME/apps
    export DART_SDK="/usr/local/Cellar/dart/2.7.1/libexec/bin"
    export JAVA_HOME=$APPLICATIONS_DIR/jre
    source ~/Documents/dotfiles/shell/Darwin/exports.sh
fi

# Go
export GOROOT=$APPLICATIONS_DIR/go
export GOBIN=$GOROOT/bin
export GOPATH=$HOME/Workspace/golang

# Rust
export RUSTBIN=$HOME/.cargo/bin

# Python
export PIP_DEFAULT_TIMEOUT=100
export PIP_DOWNLOAD_CACHE=$HOME/.pip/cache
export PYENV_ROOT=$HOME/.pyenv
export PYTHONDONTWRITEBYTECODE=1

# Fix python unknown locale: UTF-8
export LANG="en_US.UTF-8"
export LC_COLLATE="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_MESSAGES="en_US.UTF-8"
export LC_MONETARY="en_US.UTF-8"
export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"
export LANGUAGE="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"


# Node
export NVM_DIR="$HOME/.nvm"

# Ruby
export RBENV_ROOT=$HOME/.rbenv

# Java
export MAVEN_HOME=$APPLICATIONS_DIR/maven2
export M2_REPO=$HOME/.m2/repository
export M3_HOME=$APPLICATIONS_DIR/maven3
export MAVEN_OPTS="-Xms512m -Xmx2048m"

# Android
export ANDROID_HOME=${HOME}/Android/Sdk
export ANDROID_SDK_ROOT=$ANDROID_HOME
export ANDROID_STUDIO=$APPLICATIONS_DIR/android-studio
export CAPACITOR_ANDROID_STUDIO_PATH=$ANDROID_STUDIO

# Flutter
export FLUTTER_HOME=${APPLICATIONS_DIR}/flutter

# asdf
export ADSF_ROOT="$HOME/.asdf"

# QT qmake
#export QT_ROOT=/usr/lib/x86_64-linux-gnu/qt5
export QT_ROOT=/opt/qt5/5.11.2/gcc_64

# create PATH
p="$PYENV_ROOT/bin"
p+=":$RBENV_ROOT/bin"
p+=":/usr/local/cuda-12.4/bin"
p+=":/usr/local/bin:/usr/local/heroku/bin"
p+=":/snap/bin"
p+=":$APPLICATIONS_DIR"
p+=":$GOROOT/bin"
p+=":$GOPATH/bin"
p+=":$RUSTBIN"
p+=":$JAVA_HOME/bin"
p+=":$APPLICATIONS_DIR/activator" #play framework
p+=":$MAVEN_HOME/bin"             #maven 2
p+=":$M3_HOME/bin"                #maven 3
p+=":$QT_ROOT/bin"
p+=":$FLUTTER_HOME/bin"           # flutter
p+=":$HOME/.pub-cache/bin"        # flutter
p+=":$ANDROID_HOME/platform-tools"
p+=":$ANDROID_HOME/emulator"
p+=":$ANDROID_HOME/cmdline-tools/latest/bin"
p+=":$ANDROID_HOME/build-tools/34.0.0"
p+=":$ANDROID_HOME/tools"
p+=":$ANDROID_HOME/tools/bin"
p+=":$ANDROID_STUDIO/bin"
p+=":$ANDROID_STUDIO/gradle/bin"
p+=":$APPLICATIONS_DIR/adt/sdk/platform-tools"
p+=":$APPLICATIONS_DIR/adt/sdk/tools"
p+=":$APPLICATIONS_DIR/wireshark/bin"
p+=":$APPLICATIONS_DIR/google-cloud-sdk/bin"
p+=":$APPLICATIONS_DIR/seafile/bin"
p+=":/var/lib/snapd/snap/bin"
p+=":$HOME/.local/bin"
export PATH="$p:$PATH"

# Load env tools
if [ -d "$PYENV_ROOT" ]; then eval "$(pyenv init --path)"; fi
if [ -d "$PYENV_ROOT/plugins/pyenv-virtualenv" ]; then eval "$(pyenv init -)"; fi

# if [ -d "$NVM_DIR" ]; then source "$NVM_DIR/nvm.sh"; fi
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

if [ -d "$HOME/.linuxbrew" ]; then eval $(~/.linuxbrew/bin/brew shellenv); fi

if [ -d "$RBENV_ROOT" ]; then
  # eval "$(rbenv init - --no-rehash zsh)";
  export PATH="${RBENV_ROOT}/shims:${PATH}"
  export RBENV_SHELL=${SHELL}
  if [ -f "${RBENV_ROOT}/completions/rbenv.zsh" ]; then source "${RBENV_ROOT}/libexec/../completions/rbenv.zsh"; fi
  command rbenv rehash 2>/dev/null
  function rbenv() {
    local command
    command="${1:-}"
    if [ "$#" -gt 0 ]; then
      shift
    fi

    case "$command" in
    rehash|shell)
      eval "$(rbenv "sh-$command" "$@")";;
    *)
      command rbenv "$command" "$@";;
    esac
  }
fi

if [ -d "$ASDF_ROOT" ]; then
  source $ASDF_ROOT/asdf.sh
  source $ASDF_ROOT/completions/asdf.bash
fi

alias rbenv='RUBY_CONFIGURE_OPTS=--disable-install-doc rbenv'

# Nubank
export NU_HOME=$HOME/Workspace/nubank
export NUCLI_HOME=$NU_HOME/nucli
export NUDEV_HOME=$NU_HOME
if [ -d "$NUCLI_HOME" ]; then
    autoload -Uz compinit bashcompinit && compinit && bashcompinit
    source $NUCLI_HOME/nu.bashcompletion
    export NU_COUNTRY=br
    export PATH=${NUCLI_HOME}:${PATH}
fi

[ -f ~/Documents/secrets/globo/vpn ] && source ~/Documents/secrets/globo/vpn
