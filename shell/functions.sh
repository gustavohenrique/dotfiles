function strtohex {
    echo -n "${1}" | od -A n -t x1
}

function hextostr {
    echo -n "${1}" | xxd -r -p
}

function len {
    echo -n "${1}" | wc -c
}

function goenv {
    case "$1" in
        "use") ln -snf ${HOME}/Applications/go$2 ${HOME}/Applications/go ;;
        "versions"|"ls") /bin/ls -1 ~/Applications | grep '^go[0-9]' ;;
        *) echo "Usage: $0 (use|versions). Ex.: $0 use 1.16" ;;
    esac
}

function dockerun {
    local image=$1
    case "$image" in
    "postgres"|"postgresql") docker run -d -e POSTGRES_DB=$2 -e POSTGRES_USER=root -e POSTGRES_PASSWORD=root -p 5432:5432 postgres:alpine ;;
    "mysql2") docker run -d -e MYSQL_ROOT_PASSWORD=root -e MYSQL_PASSWORD=admin -e MYSQL_USER=admin -e MYSQL_DATABASE=$2 -p 3306:3306 mysql ;;
    "mysql") docker run -d -e PASS=root -e USER=root -e DB=$2 -p 3306:3306 gustavohenrique/mysql ;;
    *) echo "Image is not supported." ;;
    esac
}

function dockerip {
    docker inspect --format '{{ .NetworkSettings.IPAddress }}' "$@"
}

function dockerstats {
    docker stats --all --format "table {{.Container}}\t{{.CPUPerc}}\t{{.MemUsage}}" $1
}

function whoisbr {
    set +x 
    resp=$(curl -XPOST -d "qr=$1" "https://registro.br/2/whois#lresp") # | python3 -c "import sys, json; print (json.load(sys.stdin)['available'])")
    echo $resp
    if [ "$resp" == 0 ]; then
        echo "Disponivel";
    else
        echo "Indisponivel";
    fi
}

function denter {
    docker exec -it "$@" sh
}

function rmvolumes {
    docker volume ls | awk -F ' ' '{print $2}' | egrep -v '^[zup,mark]' | xargs docker volume rm
}

function loadenv {
    case "$1" in
	#"virtualenv") source /usr/local/bin/virtualenvwrapper.sh ;;
	"pyenv"|"python") eval "$(pyenv virtualenv-init -)"; eval "$(pyenv virtualenv-init -)" ;;
    "nvm") [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" ;;
	"rvm") [[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm" && export PATH="$PATH:$HOME/.rvm/bin";;
	"rbenv") eval "$(rbenv init -)" ;;
	*) echo "Usage load <virtualenv|nvm|rvm>" ;;
    esac
}

function heroku-bkp-postgres {
    heroku pg:backups capture --app $1
	curl -o latest.dump `heroku pg:backups public-url`
}

function start_tmux {
    if type tmux > /dev/null; then
        #if not inside a tmux session, and if no session is started, start a new session
        if [ -z "$TMUX" ]; then
            tmux new-session
        fi
    fi
}

function whatsport {
    sudo lsof -t -i tcp:$1
    # linux: sudo fuser -k 8000/tcp
}

function youtube-download-playlist {
    playlistId=${1}
    youtube-dl -i --playlist-start 0 --extract-audio --audio-format mp3 --embed-thumbnail -o '%(playlist_index)s-%(title)s.%(ext)s' "https://www.youtube.com/playlist?list=${playlistId}"
}

function youtube2mp3 {
    url=${1}
    youtube-dl --extract-audio --audio-format mp3 --embed-thumbnail -o '%(title)s.%(ext)s' "${url}"
}

function agrep {
    if [ "$1" == "-h" ]; then
        echo "Usage: agrep <dir> <expression>"
    else
        grep -r -- "$2" "$1"
    fi
}

function replace {
    if [ $(uname -s) = "Darwin" ]; then
        export LC_ALL=C
        find ./ -type f -exec sed -i '' 's,'$1','$2',g' {} \;
    else
        find ./ -type f -exec sed -i 's+'$1'+'$2'+g' {} \;
    fi
}

function renameall {
    if [ $(uname -s) = "Darwin" ]; then
        export LC_ALL=C
        find ./ -exec rename 's,'$1','$2',g' {} \;
    else
        find ./ -depth -execdir rename $1 $2 {} \;
    fi
}

function pgdb {
    docker run -d --name postgres -e POSTGRES_USER=root -e POSTGRES_PASSWORD=root -e POSTGRES_DB=$1 -p 5432:5432 postgres && echo "docker exec -it postgres psql -U root $1"
}

function mysqldb {
    docker run -d --name mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=$1 mysql
}

function decode {
    echo -n "$1" | base64 -d
}

bookmarks="${HOME}/bookmarks"
function badd {
    local exists=`egrep "${PWD}\$" ${bookmarks}`
    [[ "${exists}" != "" ]] && return
    echo ${PWD} >> ${bookmarks}
    echo "${PWD}... [added]"
}

function bdel {
    egrep -v "${PWD}\$" ${bookmarks} > /tmp/.bookmarks
    mv /tmp/.bookmarks ${bookmarks}
    echo "${PWD}... [removed]"
}

function bgo {
    local list=`sort ${bookmarks} | nl -w1`
    echo -e "\n0\tquit\n${list}"
    echo -n "Enter a directory number: "
    read selected
    if [[ ${selected} =~ ^[0-"$(echo "${list}" | wc -l)"]$ ]]; then
        [[ ${selected} = "0" ]] && return
        local dest=`grep ^${selected} <<<${list} | cut -f2`
        cd ${dest}
    else
        echo "Wrong number, stupid!"
    fi
}

function brilho {
    if (( $(echo "$1>=0 && $1<=1" | bc -l) )); then
        video=`xrandr -q | grep " connected" | awk -F ' ' '{print $1}'`
        echo "Brilho $1 no adaptador $video"
        xrandr --output $video --brightness $1
    else
        echo "Brilho deve ser de 0.1 a 1"
    fi
}

function tsh {
    ssh $1 -t "tmux new -A -s $2"
}

function clonemy {
    repo=${1}
    git clone git@github.com:gustavohenrique/${repo}
}

function httpserver {
    port=${1:-"8000"}
    echo "Listening on ${port}"
    if test -x `which caddy`; then
        caddy file-server -listen ":${port}" -browse
    else
        python -m http.server ${port}
    fi
}

function bkpcam {
    datetimes=`/bin/ls -1 | cut -c -8`
    filenames=`echo ${datetimes} | sort | uniq`
    for filename in `echo ${filenames}`; do
        total=`echo ${datetimes} | grep "${filename}" | wc -l`
        if [ "${total}" -ge "5" ]; then
            folder=`date -d "${filename}" "+%Y-%m-%d"`
            mkdir ${folder} 2>/dev/null || echo
            mv ${filename}_* ${folder}/
        fi
    done
}

function copy2s3 {
    local folder=${1}
    local year=${2}
    if [ -z "${year}" ]; then
        local year=`date '+%Y'`
    fi
    aws --profile personal s3 sync ${folder} s3://gh-personal-assets/${year}/${folder}
}

function s3-help {
    echo "
    copy2s3 2022-00-00_name  # <year>
    cd gatos && aws --profile personal s3 sync . s3://gh-personal-assets/2022/2022-00-00_genericas/gatos/
    aws --profile personal s3 sync . s3://gh-personal-assets/AdobeLightroom/
    "
}

function sync2gitlab {
    local company=${1}
    local dirs=${2}
    echo "Syncing ${company}"
    for i in `/bin/ls -1 -d ${dirs}/*/`; do
        project=`basename ${i}`;
        echo "> ${project}"
        cd ${i}
        git remote add gitlab git@gitlab.com:${company}/${project}.git
        branch=`git branch --show-current`
        git pull origin ${branch}
        git push gitlab ${branch}
        echo
    done
}

DOCS_DIR="${HOME}/Documents"
function secretson {
    sudo losetup /dev/loop1 "${DOCS_DIR}/secrets.img"
    sudo cryptsetup open /dev/loop1 secrets
    sudo mount /dev/mapper/secrets "${DOCS_DIR}/secrets"
}

function secretsoff {
    sudo umount ${DOCS_DIR}/secrets
    sudo cryptsetup close secrets
    sudo losetup -d /dev/loop1
}

# add_prefix adiciona prefixo numerico incremental nos arquivos com a ext fornecida
# add_prefix *.mp4 = 01_video.mp4 02_another-video.mp4
function add_prefix {
  if [ -z "$1" ]; then
    echo "Uso: $0 <extensao>"
    echo "Ex.: $0 mp4"
    return 1
  fi

  local EXTENSAO=$1
  local PASTA="."
  # local arquivos=($(find "$PASTA" -maxdepth 1 -type f -name "*.$EXTENSAO" -printf "%T@ %p\n" | sort -n | awk -F './' '{print $2}'))
  local arquivos=()
  while IFS= read -r -d '' linha; do
    arquivos+=("$linha")
  done < <(find "$PASTA" -maxdepth 1 -type f -name "*.$EXTENSAO" -printf "%T@ %p\0" | sort -z -n)

  local contador=1
  for arquivo in "${arquivos[@]}"; do
    local prefixo=$(printf "%02d" $contador)
    local nome_arquivo=$(basename "$arquivo")
    if [[ "$nome_arquivo" =~ ^[0-9]{2}_ ]]; then
      echo "Arquivo '$nome_arquivo' já possui prefixo numérico. Ignorando."
      continue
    fi
    local novo_nome="${prefixo}_$nome_arquivo"
    mv "$nome_arquivo" "$novo_nome"
    echo "Arquivo '$novo_nome'"
    contador=$((contador + 1))
  done
}

function nc-server {
    port=${1:-"8000"}
    echo "Listening on ${port}"
    ncat -lnvp ${port} --ssl
}

function socat-server {
    port=${1:-"8000"}
    openssl req -new -x509 -keyout /tmp/test.key -out /tmp/test.crt -nodes
    cat /tmp/test.key /tmp/test.crt > /tmp/test.pem
    echo "Listening on ${port}"
    socat openssl-listen:${port},reuseaddr,cert=/tmp/test.pem,verify=0,fork stdio
}
